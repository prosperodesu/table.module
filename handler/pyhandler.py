import csv
import sys
import os
import uuid
import xlrd
import threading

import mysql.connector
from ConfigParser import SafeConfigParser

conn = mysql.connector.connect(host='localhost',
                                       database='tables',
                                       user='root',
                                       password='root')

cursor = conn.cursor()

command_args = sys.argv
filename = command_args[1]
extension = command_args[2]
session_id = command_args[3]
current_path = os.getcwd() + '/handler'
queueFileName = str(uuid.uuid4()) + '.queue'
sql_body = "INSERT INTO cities(country_id, city, state, region, biggest_city) VALUES (%s, %s, %s, %s, %s)"

cursor.execute("INSERT INTO tasks (session_id) VALUES('%s')" % session_id)
conn.commit()
task_id = cursor.lastrowid

try:
    os.remove(os.getcwd() + '/cache/' + 'total_records' + '.cache')
except Exception:
    pass

def get_csv_data(csv_fname):
  with open(csv_fname, "r") as records:
    for record in csv.reader(records):
      try:
        yield record
      except Exception:
        print Exception

def read_in_chunks(file_object, chunk_size=100000):
    """Lazy function (generator) to read a file piece by piece.
    Default chunk size: 100k."""
    while True:
        data = file_object.readlines(chunk_size)
        if not data:
            break
        yield data

def make_sql_values(values):
    values = values.replace('\r', '')
    values = values.replace('\n', '')
    return values.split(',')

if extension == 'csv':
    csv_file = open(filename, 'r')
    csv_num_lines_file = open(filename, 'r')
    csv_len = len(csv_num_lines_file.readlines())
    cursor.execute("UPDATE tasks SET rows_total = %s WHERE id = %s" % (csv_len, task_id))
    conn.commit()
    rows_inserted = 0
    for rows in read_in_chunks(csv_file):
        try:
            sql_values = map(make_sql_values, rows)
            cursor.execute("UPDATE tasks SET rows_imported = %s WHERE id = %s" % (rows_inserted, task_id))
            cursor.executemany(sql_body, sql_values)
            conn.commit()
            print str(rows_inserted) + '/' + str(csv_len)
            rows_inserted = rows_inserted + cursor.rowcount
            try:
                os.remove(os.getcwd() + '/cache/' + 'total_records' + '.cache')
            except Exception:
                pass
        except Exception as e:
            print e
            continue

    cursor.execute("UPDATE tasks SET is_finished = 1 WHERE id = %s" % task_id)
    conn.commit()

if extension == 'xls' or 'xlsx':
    book = xlrd.open_workbook(filename)
    sh = book.sheet_by_index(0)
    counter = 0
    cursor.execute("UPDATE tasks SET rows_total = %s WHERE id = %s" % (len(range(sh.nrows)), task_id))
    conn.commit()
    for rx in range(sh.nrows):
        try:
            rows = sh.row_values(rx)
            counter = counter + 1
            cursor.execute("UPDATE tasks SET rows_imported = %s WHERE id = %s" % (counter, task_id))
            cursor.execute("INSERT INTO cities (country_id, city, state, region, biggest_city) VALUES ('%s', '%s', '%s', '%s', '%s')" % (rows[0], rows[1], rows[2], rows[3], rows[4]))
            conn.commit()
            print e
            try:
                os.remove(os.getcwd() + '/cache/' + 'total_records' + '.cache')
            except Exception:
                pass
        except Exception:
            print Exception
            continue
    cursor.execute("UPDATE tasks SET is_finished = 1 WHERE id = %s" % task_id)
    conn.commit()