<?php

namespace src\config;

/**
 * Class Config
 *
 * Configuration class for get target param from settings file
 */
class Config
{
    protected $config;

    public function __construct()
    {
        $this->config = parse_ini_file("config/settings.ini", true);
    }

    /**
     * @param $type
     * @param $param
     * @return mixed
     * @throws \Exception
     *
     * Return config param for target section
     */
    public function getParam($type, $param)
    {
        if (isset($this->config[$type][$param])) {
            return $this->config[$type][$param];
        } else {
            throw new \Exception('Undefined section or param');
        }
    }
}