<?php

namespace src\cache;

class Cache
{
    protected $filePath;
    public $value;
    public $key;

    public function __construct($key, $value)
    {
        $this->filePath = getcwd() . '/cache/' . $key . '.cache';
        $file = fopen($this->filePath, 'w');
        fwrite($file, serialize($value));

        $this->key = $key;
        $this->value = $value;
    }

    public static function isCacheExist($key)
    {
        $path = getcwd() . '/cache/' . $key . '.cache';
        if (file_exists($path)) {
            return true;
        } else {
            return false;
        }
    }

    public static function findOne($key)
    {
        if (static::isCacheExist($key)) {
            $value = file_get_contents(getcwd() . '/cache/' . $key . '.cache');
            return unserialize($value);
        } else {
            return false;
        }
    }

    public static function flush($key)
    {
        if (static::isCacheExist($key)) {
            unlink(getcwd() . '/cache/' . $key . '.cache');
        }
    }
}