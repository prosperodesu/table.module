<?php

namespace src\fparse;

class PyHandler
{
    public static function run($path, $filePath, $ext)
    {
        session_start();

        if (isset($_SESSION['tasks_save_id'])) {
            $task_save_id = $_SESSION['tasks_save_id'];
        } else {
            $task_save_id = uniqid();
            $_SESSION['tasks_save_id'] = $task_save_id;
        }

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            shell_exec("start /B python {$path}/handler/pyhandler.py '{$filePath}' '{$ext}' '{$task_save_id}'");
        } else {
            shell_exec("nohup python {$path}/handler/pyhandler.py '{$filePath}' '{$ext}' '{$task_save_id}' > /dev/null 2>&1 &");
        }
    }
}