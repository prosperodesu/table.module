<?php

namespace src\fparse;

use src\fparse\ParseCsv;
use lib\excel\PHPExcel\PHPExcel_IOFactory;
use Akeneo\Component\SpreadsheetParser\SpreadsheetParser;


class ParseExcel extends ParseCsv
{
    protected function makeArrayData($path)
    {
        try {
            if ($this->extension == 'xlsx') {
                $workbook = SpreadsheetParser::open($path);

                foreach ($workbook->createRowIterator(0) as $rowIndex => $values) {
                    $this->arrayData[] = $values;
                }
            } else {
                $document = PHPExcel_IOFactory::load($path);

                $activeSheetData = $document->getActiveSheet()->toArray(null, true, true, false);

                $this->arrayData = $activeSheetData;
            }

        } catch (\Exception $e) {
            echo $e;
        }
    }
}