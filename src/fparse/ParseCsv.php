<?php

namespace src\fparse;

use src\models\Table;
use Akeneo\Component\SpreadsheetParser\SpreadsheetParser;

class ParseCsv
{
    protected $arrayData = [];
    protected $result = [];
    protected $columns = [];
    protected $firstLineIsColumns = false;
    protected $ignoreId;
    protected $query;
    protected $extension;

    public function __construct($path, $columns, $ignoreId, $firstLineColumns, $extension = false)
    {
        $this->extension = $extension;

        $this->makeArrayData($path);

        $this->columns = $columns;

        $this->firstLineIsColumns = $firstLineColumns;

        if ($this->firstLineIsColumns) {
            $this->columns = $this->arrayData[0];
            array_splice($this->arrayData, 0, 1);
        }

        $this->query = $this->batchImport();
    }

    protected function makeArrayData($path)
    {

        $workbook = SpreadsheetParser::open($path);

        foreach ($workbook->createRowIterator(0) as $rowIndex => $values) {
            $this->arrayData[] = $values;
        }


    }

    protected function batchImport()
    {
        $columns = implode(',', $this->columns);

        $table = Table::tableName();

        $valuesAll = [];

        foreach ($this->arrayData as $row) {
            if ($row) {
                $newRows = array_map(function($value) {
                    return "'{$value}'";
                }, $row);
                $values = implode(',', $newRows);
                $valuesAll[] = "({$values})";
            }
        }

        $values = implode(',', $valuesAll);

        $query = "INSERT INTO {$table}($columns) VALUES $values";

        return $query;
    }

    public function getQuery()
    {
        return $this->query;
    }
}