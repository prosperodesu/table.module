<?php

namespace src\models;

use src\database\Connection;

/**
 *
 * Base model class
 * Class BaseModel
 * @package src\models
 */
class BaseModel
{
    protected $connection;
    protected $fields;
    protected static $queryParams = ['asArray' => false, 'limit' => false, 'rows' => '*', 'params' => []];
    protected static $query;

    public function __construct()
    {
        $instance = new Connection();

        $this->connection = $instance->getConnection();
        $this->setAttributes();
    }

    /**
     * @return string
     */
    protected static function getTableName()
    {
        return static::tableName();
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return substr(strrchr('\\' . get_called_class(), '\\'), 1);
    }

    /**
     * @return string
     */
    public static function className()
    {
        return get_called_class();
    }

    protected function setAttributes()
    {
        $fields = self::getAttributes();

        foreach ($fields as $field) {
            $this->{$field} = null;
            $this->fields[] = $field;
        }

    }

    /**
     * @return array
     */
    public static function getAttributes()
    {


        $tableName = static::getTableName();

        $instance = new Connection();
        $connection = $instance->getConnection();

        $query = $connection->prepare("DESCRIBE {$tableName}");
        $query->execute();
        $fields = $query->fetchAll(\PDO::FETCH_COLUMN);

        return $fields;
    }

    /**
     * @return array
     *
     * Get and return params for table
     */
    public function getParams()
    {
        $parameters = array(
            'table' => static::getTableName(),
            'searchFields' => static::searchFields(),
            'tableName' => static::getTableName(),
            'columns' => static::getAttributes(),
            'columnTypes' => array('varchar', 'varchar', 'int', 'float'),
            'columnNames' => static::getAttributes(),
            'globalFilter' => "column1 like 'pr%' and column3 > 0",
            'features' => array('view', 'filter', 'edit', 'delete', 'sort', 'downloadCSV', 'downloadExcel', 'downloadPDF'
            , 'print', 'copyToClipboard', 'uploadCSV', 'uploadExcel', 'truncate')
        );

        return $parameters;
    }

    /**
     * @return static
     */
    public static function find()
    {
        $tableName = static::getTableName();


        static::$queryParams['columns'] = '*';
        static::$queryParams['table'] = $tableName;

        return new static;
    }

    /**
     * @return static
     */
    public function asArray()
    {
        static::$queryParams['asArray'] = true;

        return new static;
    }

    /**
     * @param $limit
     * @return static
     */
    public function limit($limit)
    {
        static::$queryParams['limit'] = $limit;

        return new static;
    }

    /**
     * @param $start
     * @return static
     */
    public function offset($start)
    {
        static::$queryParams['offset'] = $start;

        return new static;
    }

    /**
     * @param $condition
     * @return static
     */
    public function where($condition)
    {
        static::$queryParams['where'] = 'WHERE ' . $condition;

        return new static;
    }

    public static function bindParams($params = [])
    {
        static::$queryParams['params'] = $params;

        return new static;
    }


    public function count()
    {
        $lastParam = static::$queryParams['rows'];

        static::$queryParams['rows'] = 'COUNT(*) as count';

        $sql = $this->executeQuery();

        $data = $sql->fetch(\PDO::FETCH_NUM);

        $count = $data[0];

        static::$queryParams['rows'] = $lastParam;

        return (int)$count;
    }

    protected function makeQuery()
    {
        $tableName = static::$queryParams['table'];

        if (static::$queryParams['limit']) {
            $limit = static::$queryParams['limit'];
            if (static::$queryParams['offset']) {
                $offset = static::$queryParams['offset'];
                $limit = "LIMIT {$offset},{$limit}";
            } else {
                $limit = "LIMIT 0,{$limit}";
            }

        } else {
            $limit = '';
        }

        if (static::$queryParams['where']) {
            $where = static::$queryParams['where'];
        } else {
            $where = '';
        }

        $rows = static::$queryParams['rows'];

        static::$query = "SELECT {$rows} FROM `{$tableName}` {$where} {$limit}";
    }

    protected function executeQuery()
    {
        self::makeQuery();

        $instance = new Connection();
        $connection = $instance->getConnection();

        $params = static::$queryParams['params'];

        $sql = $connection->prepare(static::$query);

        $sql->execute($params);

        return $sql;
    }

    /**
     * @return array
     */
    public function all()
    {
        $attributes = self::getAttributes();

        $class = self::className();

        $rows = [];

        $sql = $this->executeQuery();

        while ($row = $sql->fetch(\PDO::FETCH_LAZY)) {

            if (!static::$query['asArray']) {
                $object = new $class;

                foreach ($attributes as $attribute) {

                    $object->{$attribute} = $row->{$attribute};
                }

                $rows[] = $object;
            } else {

                $item = [];

                foreach ($attributes as $attribute) {

                    $item[] = $row->{$attribute};
                }

                $rows[] = $item;
            }

        }

        return $rows;
    }

    public static function searchFields()
    {
        return ['*'];
    }

    public static function importColumns()
    {
        return self::getAttributes();
    }

    public function save()
    {
        $tableName = static::getTableName();

        $attributesGet = static::getAttributes();

        $attributes = [];

        foreach ($attributesGet as $attribute) {
            if ($attribute != 'id') {
                $attributes[] = $attribute;
            }
        }

        $values = [];
        $valuesBind = [];

        foreach ($attributes as $attribute) {
            $values[] = $this->{$attribute};
            $valuesBind[] = '?';
        }

        $attributes = implode(',', $attributes);
        $valuesBind = implode(',', $valuesBind);

        $instance = new Connection();

        $connection = $instance->getConnection();

        if (!empty($values)) {
            static::$query = "INSERT INTO {$tableName}($attributes) VALUES($valuesBind)";
            $sql = $connection->prepare(static::$query);
            $sql->execute($values);
        }
    }

    public static function deleteAll()
    {
        $tableName = static::getTableName();

        $instance = new Connection();

        $connection = $instance->getConnection();

        $lastQuery = "SELECT id FROM {$tableName} ORDER BY id DESC LIMIT 1";
        $sql = $connection->prepare($lastQuery);
        $sql->execute();
        $row = $sql->fetch();
        $lastId = $row['id'];

        static::$query = "TRUNCATE TABLE {$tableName}";
        $sql = $connection->prepare(static::$query);
        $sql->execute();
    }

    public static function import($query)
    {
        $instance = new Connection();
        $connection = $instance->getConnection();

        $sql = $connection->prepare($query);
        $sql->execute();
    }
}