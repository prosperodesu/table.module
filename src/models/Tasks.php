<?php

namespace src\models;

use src\models\BaseModel;

/**
 * Class Table
 *
 * @package src\models
 */
class Tasks extends BaseModel
{
    public static function tableName()
    {
        return 'tasks';
    }
}