<?php

namespace src\models;

use src\models\BaseModel;

/**
 * Class Table
 *
 * @package src\models
 */
class Table extends BaseModel
{
    public static function tableName()
    {
        return 'cities';
    }

    public static function searchFields()
    {
        return ['city', 'state'];
    }

    public static function importColumns()
    {
        return ['country_id', 'city', 'state', 'region', 'biggest_city'];
    }
}