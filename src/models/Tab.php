<?php

namespace src\models;

use src\database\Connection;

/**
 * Class Tab
 * @package src\models
 *
 * Tab model
 */
class Tab
{
    public $id;
    public $name;
    public $userId;

    protected $connection;

    public function __construct()
    {
        $instance = new Connection();

        $this->connection = $instance->getConnection();
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'analytic_module_tabs';
    }

    /**
     * Save model
     */
    public function save()
    {
        $sql = $this->connection->prepare(
            "INSERT INTO {$this::tableName()} (name, user_id) VALUES(?, ?)"
        );

        $sql->execute([$this->name, $this->userId]);

        $this->id = $this->connection->lastInsertId();
    }

    /**
     * @return array
     * Find all models
     */
    public static function findAll()
    {
        $result = [];

        $instance = new Connection();
        $connection = $instance->getConnection();

        $sql = $connection->prepare("SELECT id, name, user_id FROM analytic_module_tabs");
        $sql->execute();

        while ($row = $sql->fetch(\PDO::FETCH_LAZY)) {

            $tab = new Tab();
            $tab->id = $row->id;
            $tab->name = $row->name;
            $tab->userId = $row->user_id;

            $result[] = $tab;
        }

        return $result;

    }

    /**
     * @param $id
     * @return Tab
     *
     * Find one tab by id
     */
    public static function findOne($id)
    {
        $instance = new Connection();
        $connection = $instance->getConnection();

        $sql = $connection->prepare("SELECT id, name, user_id FROM analytic_module_tabs WHERE id = ?");

        $sql->execute([$id]);

        $row = $sql->fetch();

        $tab = new Tab();
        $tab->id = $row['id'];
        $tab->name = $row['name'];
        $tab->userId = $row['user_id'];

        return $tab;
    }

    /**
     * Delete current tab
     */
    public function delete()
    {
        $sql = $this->connection->prepare(
            "DELETE FROM analytic_module_graph WHERE tab_id = ?"
        );

        $sql->execute([$this->id]);

        $sql = $this->connection->prepare(
            "DELETE FROM {$this::tableName()} WHERE id = ?"
        );

        $sql->execute([$this->id]);

    }
}