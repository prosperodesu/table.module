<?php

namespace src\router;

use src\models\Table;
use src\cache\Cache;
use src\fparse\ParseCsv;
use src\fparse\ParseExcel;
use src\fparse\PyHandler;
use src\models\Tasks;

/**
 * Class Router
 * @package src\router
 *
 * Class for router handlers
 */
class Router extends BaseRouter
{

    /**
     * Index action
     */
    public function indexAction()
    {
        if ($columns = Cache::findOne('columns')) {
        } else {
            $columns = Table::getAttributes();
            $cacheColumns = new Cache('columns', $columns);
        }

        if ($items = Cache::findOne('init_items')) {
        } else {
            $items = Table::find()
                ->limit(100)
                ->all();

            $cacheItems = new Cache('init_items', $items);
        }

        $this->render('index', ['columns' => $columns, 'items' => $items]);
    }

    protected function filterTable($asArray = true)
    {
        $data = [];

        $offset = $this->getQuery('start');
        $limit = $this->getQuery('length');

        if ($columns = Cache::findOne('columns')) {
        } else {
            $columns = Table::getAttributes();
            $cacheColumns = new Cache('columns', $columns);
        }

        if ($totalRecords = Cache::findOne('total_records')) {
        } else {
            $totalRecords = Table::find()->count();
            $cacheTotalRecords = new Cache('total_records', $totalRecords);
        }

        if (!$this->getQuery('query')['value']) {
            $criteria = $this->getQuery('search');
        } else {
            $criteria = $this->getQuery('query');
        }

        $data['columns'] = $columns;

        $searchFields = Table::searchFields();

        $where = [];
        $bind = [];

        if (!empty($criteria['value'])) {
            $value = $criteria['value'];
            $i = 0;
            foreach ($searchFields as $column) {
                if ($i == 0) {
                    if ($criteria['regex'] == "true") {
                        $where[] = " {$column} LIKE :{$column}";
                    } else {
                        $where[] = " {$column} = :{$column}";
                    }
                } else {
                    if ($criteria['regex'] == "true") {
                        $where[] = " OR {$column} LIKE :{$column}";
                    } else {
                        $where[] = " OR {$column} = :{$column}";
                    }
                }
                if ($criteria['regex'] == "true") {
                    $bind[$column] = "%{$value}%";
                } else {
                    $bind[$column] = $value;
                }
                $i++;
            }
        }

        if (!empty($criteria['value'])) {

            $totalRecords = Table::find()
                ->where(implode(' ', $where))
                ->bindParams($bind)
                ->count();
        }
        $tableData = Table::find()
            ->offset($offset)
            ->limit($limit);

        if ($where) {
            $tableData = $tableData
                ->where(implode(' ', $where))
                ->bindParams($bind);
        }

        if ($asArray) {
            $tableData = $tableData
                ->asArray()
                ->all();
        } else {
            $tableData = $tableData
                ->all();
        }

        if (!empty($criteria['value'])) {
            $data['iTotalRecords'] = $totalRecords;
            $data['iTotalDisplayRecords'] = $totalRecords;
        } else {
            $data['iTotalRecords'] = $totalRecords;
            $data['iTotalDisplayRecords'] = $totalRecords;
        }

        $data['aaData'] = $tableData;

        return $data;
    }

    public function getTableDataAction()
    {
        $data = $this->filterTable();

        $this->ajaxResponse($data);
    }

    public function searchAction()
    {
        $data = $this->filterTable(false);

        $this->render('index', ['columns' => $data['columns'], 'items' => $data['aaData']]);
    }

    public function uploadAction()
    {
        ini_set('max_execution_time', 3600);

        try {
            $allowed_extensions = ['sql', 'xls', 'xlsx', 'csv'];

            $document = $_FILES[0];

            $path_parts = pathinfo($document['name']);

            $extension = $path_parts['extension'];

            $newName = uniqid() . '.' . $extension;
            $newPath = getcwd() . '/temp/' . $newName;

            $ignoreIdPost = $_POST['ignoreId'];
            $firstLine = $_POST['firstLineColumns'];

            if ($ignoreIdPost == "true") {
                $ignoreId = true;
            } else {
                $ignoreId = false;
            }

            if ($firstLine == "true") {
                $firstLineColumns = true;
            } else {
                $firstLineColumns = False;
            }

            if (in_array($extension, $allowed_extensions)) {

                move_uploaded_file($document['tmp_name'], $newPath);

                PyHandler::run(getcwd(), $newPath, $extension);

                $clearTable = $_POST['isClearTable'];


                if ($clearTable == "true") {
                    $clearAll = true;
                } else {
                    $clearAll = false;
                }

                if ($clearAll) {
                    Table::deleteAll();
                }

                Cache::flush('total_records');

                $this->ajaxResponse(['result' => 'success']);
            }
        } catch (\Exception $e) {
            echo $e;
        }
    }

    public function renderQueueAction()
    {
        session_start();

        if (isset($_SESSION['tasks_save_id'])) {
            $task_save_id = $_SESSION['tasks_save_id'];
        } else {
            $task_save_id = uniqid();
            $_SESSION['tasks_save_id'] = $task_save_id;
        }

        $tasks = Tasks::find()
            ->where('session_id = :session_id AND is_finished = 0')
            ->bindParams(['session_id'=>$task_save_id])
            ->asArray()
            ->all();

        $this->ajaxResponse($tasks);
    }
}