<?php

namespace src\router;

/**
 * Class BaseRouter
 * @package src\router
 *
 * Class for process url requests
 */
class BaseRouter
{
    protected $routes;
    protected $get = [];
    protected $post = [];
    protected $files = [];

    public function __construct()
    {
        $this->routes = [
            '/' => 'index',
            '/ajax/table' => 'getTableData',
            '/search' => 'search',
            '/ajax/upload' => 'upload',
            '/insert/row' => 'insertRow',
            '/ajax/get/queue' => 'renderQueue'
        ];
    }

    /**
     * @param $path
     * @throws \Exception
     *
     * Call target method for path
     */
    public function path($path)
    {
        if (isset($this->routes[$path])) {
            $handler = $this->routes[$path] . 'Action';
            if ($_GET) {
                $this->get = $_GET;
            }
            if ($_POST) {
                $this->post = $_POST;
            }
            if ($_FILES) {
                $this->files = $_FILES;
            }
            $this->{$handler}();
        } else {
            header("HTTP/1.0 404 Page not found");
            http_response_code(404);
        }
    }

    /**
     * @param $template
     * @param array $params
     *
     * Render target template
     */
    protected function render($template, $params = [])
    {
        ob_start();
        extract($params, EXTR_SKIP);

        include("templates/{$template}.template.php");
    }

    /**
     * @param $data
     *
     * Return ajax request
     */
    protected function ajaxResponse($data)
    {
        header('Content-Type: application/json');
        echo json_encode($data);
        die();
    }

    /**
     * @param $param
     * @return mixed
     * Get post request
     */
    protected function getPost($param)
    {
        return $this->post[$param];
    }

    protected function getQuery($param)
    {
        return $this->get[$param];
    }

    protected function getFile($file)
    {
        return $this->files[$file];
    }
}