<?php

namespace src\database;

use src\database\Connection;
use src\models\Tab;

/**
 * Class Migration
 * @package src\database
 *
 * Init database when app first install or add some features
 */
class Migration
{
    protected $connection;

    public function __construct()
    {
        $instance = new Connection();

        $this->connection = $instance->getConnection();
    }

    public function migrate()
    {
        $this->initMigration();
        $this->createTabsTable();
        $this->createGraphsTable();
    }

    protected function writeMigrationHistory($name)
    {
        $sql = $this->connection->prepare(
            "INSERT INTO analytic_module_migrations(name) VALUES(?)
        ");

        $sql->execute([$name]);
    }

    protected function isMigrationExists($name)
    {
        $sql = $this->connection->prepare(
            "SELECT id from analytic_module_migrations WHERE name = ?"
        );

        $sql->execute([$name]);

        $id = $sql->fetchColumn();

        if ($id) {
            return true;
        } else {
            return false;
        }
    }

    protected function initMigration()
    {
        $schema = "
            CREATE TABLE IF NOT EXISTS analytic_module_migrations(
              ID INT(11) AUTO_INCREMENT PRIMARY KEY,
              name VARCHAR(50) NOT NULL,
              applied TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
            )
        ";

        $this->connection->exec($schema);
    }

    protected function createTabsTable()
    {
        $name = 'create_tabs_table';

        $schema = "
            CREATE TABLE IF NOT EXISTS analytic_module_tabs(
              ID INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
              name VARCHAR(50) NOT NULL,
              user_id INT(11) UNSIGNED DEFAULT NULL
            )
        ";

        if (!$this->isMigrationExists($name)) {

            echo "Apply migration {$name}..." . PHP_EOL;

            $this->writeMigrationHistory($name);
            $this->connection->exec($schema);
        }
    }

    protected function createGraphsTable()
    {
        $name = 'create_graphs_table';

        $schema = "
            CREATE TABLE IF NOT EXISTS analytic_module_graph(
              ID INT(11) AUTO_INCREMENT PRIMARY KEY,
              name VARCHAR(50) NOT NULL,
              type VARCHAR(30) NOT NULL,
              data LONGTEXT DEFAULT NULL,
              titles LONGTEXT DEFAULT NULL,
              tab_id INTEGER(11) UNSIGNED,
              FOREIGN KEY (tab_id) REFERENCES analytic_module_tabs(id)
            )
        ";

        if (!$this->isMigrationExists($name)) {

            echo "Apply migration {$name}..." . PHP_EOL;

            $this->writeMigrationHistory($name);
            $this->connection->exec($schema);
        }
    }
}