<?php

namespace src\database;

use src\config\Config;

/**
 * Class Connection
 * @package src\database
 *
 * Class for connect to database
 */
class Connection
{
    protected $pdo;

    public function __construct()
    {
        $config = new Config();

        $host = $config->getParam('database', 'host');
        $db = $config->getParam('database', 'database');
        $charset = $config->getParam('database', 'charset');
        $user = $config->getParam('database', 'username');
        $password = $config->getParam('database', 'password');

        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";

        $opt = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES => false,
        ];

        $this->pdo = new \PDO($dsn, $user, $password, $opt);
    }

    /**
     * @return \PDO
     *
     * Return current connection
     */
    public function getConnection()
    {
        return $this->pdo;
    }
}