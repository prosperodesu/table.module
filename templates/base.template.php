<html>
<head>

    <meta charset="UTF-8">
    <title>Tables Module</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>

    <script src="/static/dist/js/jquery-3.2.0.min.js"></script>

    <script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">

    <link rel="stylesheet" href="/static/css/scroller.dataTables.min.css">

    <script src="/static/dist/js/dataTables.scroller.min.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script src="/static/scripts/angular/app.js"></script>

    <style>
        .chat-box {
            font:normal normal 11px/1.4 Tahoma,Verdana,Sans-Serif;
            color:#333;
            width:400px; /* Chatbox width */
            border:1px solid #344150;
            border-bottom:none;
            background-color:white;
            position:fixed;
            right:10px;
            bottom:0;
            z-index:9999;
            -webkit-box-shadow:1px 1px 5px rgba(0,0,0,.2);
            -moz-box-shadow:1px 1px 5px rgba(0,0,0,.2);
            box-shadow:1px 1px 5px rgba(0,0,0,.2);
        }

        .chat-box > input[type="checkbox"] {
            display:block;
            margin:0 0;
            padding:0 0;
            position:absolute;
            top:0;
            right:0;
            left:0;
            width:100%;
            height:26px;
            z-index:4;
            cursor:pointer;
            opacity:0;
            filter:alpha(opacity=0);
        }

        .chat-box > label {
            display:block;
            height:24px;
            line-height:24px;
            background-color:#344150;
            color:white;
            font-weight:bold;
            padding:0 1em 1px;
        }

        .chat-box > label:before {content:attr(data-collapsed)}

        .chat-box .chat-box-content {
            padding:10px;
            display:none;
        }

        /* hover state */
        .chat-box > input[type="checkbox"]:hover + label {background-color:#404D5A}

        /* checked state */
        .chat-box > input[type="checkbox"]:checked + label {background-color:#212A35}
        .chat-box > input[type="checkbox"]:checked + label:before {content:attr(data-expanded)}
        .chat-box > input[type="checkbox"]:checked ~ .chat-box-content {display:block}
    </style>

</head>
<body>

