<?php include_once('base.template.php') ?>

    <div style="height: 100%" class="container">

        <div id="dialogFile" title="Загрузить данные в таблицу">

            <form method="POST" action="/ajax/upload" id="uploadForm" enctype="multipart/form-data">

                <div>
                    <label style="margin-bottom:10px;">
                        Файл CSV или Excel
                    </label>

                    <input id="document" name="document" type="file">

                </div>

                <div style="margin-top:20px;">
                    <input type="checkbox" id="firstLineColumns" name="firstLineColumns">
                    <label>Первая строка список колонок</label>
                </div>

                <div style="margin-top:20px;">
                    <input type="checkbox" id="cleanTable" name="cleanTable">
                    <label>Очистить таблицу перед загрузкой</label>
                </div>

                <div class="progress-label"></div>

                <div id="status_upload">

                </div>

                <div id="progressbar" style="margin-top:10px;"></div>

                <input id="rows_loaded" type="hidden" value="1">

                <button type="button" id="startLoad" style="margin-top:10px;" disabled>Загрузить</button>

            </form>
        </div>

        <button id="loadDataForm">Загрузить данные</button>

        <div id="table_container">
            <table id="table_grid" class="bordered highlight responsive-table">
                <thead>
                <tr>
                    <?php foreach ($columns as $column): ?>
                        <th><?php echo $column ?></th>
                    <?php endforeach; ?>
                </tr>
                </thead>

                <tbody>

                <?php foreach ($items as $item): ?>
                    <tr>
                        <?php foreach ($columns as $column): ?>
                            <th><?php echo $item[$column] ?></th>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    </div>

    <div class="chat-box">
        <input type="checkbox"/>
        <label data-expanded="Скрыть" data-collapsed="Показать очередь"></label>
        <div class="chat-box-content">
            Нет задач в очереди
        </div>
    </div>

<?php include_once('footer.template.php') ?>