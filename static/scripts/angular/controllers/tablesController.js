app.controller('tablesController', function ($scope) {

    $("#dialogFile").dialog({'autoOpen': false});

    $scope.upload = function() {

        var values = [10, 20, 30, 50, 70, 80, 90, 100];

        values.forEach(function (value) {
            $("#progressbar").progressbar({
                value: value
            });
        });
    };

    $scope.showAddFileForm = function() {
        $("#showAddFileForm").dialog('show');
    };

});