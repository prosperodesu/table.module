$(document).ready(function () {

    function refreshQueue() {
        $.ajax({
            'url': '/ajax/get/queue',
            'type': 'GET',
            'success': function (data) {
                $('.chat-box-content').html('');
                data.forEach(function (item) {
                    $('.chat-box-content').append(
                        $('<span>').html('Задача: ' + item[0] + '|Загружено строк: ' + item[2] + '/' + item[6])
                    );
                    $('.chat-box-content').append(
                        $('<br>')
                    )
                });
            }
        })
    }

    setInterval(refreshQueue, 2000);

    $("#dialogFile").dialog({
        'autoOpen': false,
        'width': 800,
        'height': 600
    });

    $('#document').change(function () {
        $('#startLoad').removeAttr('disabled');

        $('#startLoad').click(function () {
            previewFile();
        });
    });

    $("#progressbar").progressbar({
        value: 0,
        create: function (event, ui) {
            $(this).find('.ui-widget-header').css({'background-color': 'green', 'border': 'none'});
            $(this).find('.ui-widget-content').css({'border': 'none'})
        }
    });

    var loadItem = function (item_number, items_length, row) {

        $.ajax({
            'url': 'insert/row',
            'data': {
                'total': items_length,
                'number': item_number,
                'row': row
            },
            'type': 'POST',
            'success': function (response) {
                changePercent(Math.round(response.percent));
                if (items_length == item_number) {
                    location.reload();
                }
            }
        });
    };

    var changePercent = function (percent) {
        if (percent >= 99) {
            $('#status_upload').html('<center>Обработка файла...</center>');
        }
        $("#progressbar").progressbar("value", percent);
    };


    function previewFile() {

        $('#startLoad').hide();

        var data = new FormData($('input[name^="document"]'));

        jQuery.each($('input[name^="document"]')[0].files, function (i, file) {
            data.append(i, file);
        });

        data.append('ignoreId', $('#ignoreId').is(":checked"));
        data.append('firstLineColumns', $('#firstLineColumns').is(":checked"));
        data.append('isClearTable', $('#cleanTable').is(":checked"));

        if ($('#document').val()) {

            $.ajax({
                'url': '/ajax/upload',
                'type': 'POST',
                'data': data,
                'cache': false,
                'contentType': false,
                'processData': false,
                'xhr': function () {
                    var xhr = $.ajaxSettings.xhr();

                    if (xhr.upload) {
                        xhr.upload.addEventListener('progress', function (event) {

                            var percent = 0;
                            var position = event.loaded || event.position;
                            var total = event.total;
                            if (event.lengthComputable) {
                                percent = Math.ceil(position / total * 100);
                            }
                            changePercent(percent)

                        }, true);
                    }

                    return xhr;
                },
                'success': function (data) {
                    $('#status_upload').html('<center>Добавлено в очередь</center>');
                    location.reload();
                }, 'error': function (error) {
                    console.log(error);
                }
            });
        }
    }

    $('#loadDataForm').click(function () {
        $("#dialogFile").dialog('open');
    });

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    /*$('#table_grid thead th').each(function () {
     var title = $('#table_grid thead th').eq($(this).index()).text();
     $(this).append('<br> <input type="text" placeholder="Search ' + title + '" />');
     });*/

    if (getParameterByName("search[value]")) {
        if (getParameterByName("search[regex]") == "true") {
            var url = "/ajax/table?query[value]=" + getParameterByName("search[value]") + '&query[regex]=true';
        } else {
            var url = "/ajax/table?query[value]=" + getParameterByName("search[value]");
        }
    } else {
        var url = '/ajax/table';
    }
    var table = $('#table_grid').DataTable({
        serverSide: true,
        ordering: false,
        scrollY: 600,
        scroller: {
            loadingIndicator: true
        },
        deferRender: true,
        'ajax': {
            'url': url,
            "type": "GET"
        }
    });

    var input = $('.dataTables_filter').find('label').find('input');

    input.unbind();

    $('.dataTables_filter')
        .append(
            $('<button>')
                .attr('id', 'table-search-btn')
                .html('Search')
                .click(function () {
                    var body = $('#table_grid').find('tbody');
                    var rows = body.find('tr');
                    var isFull = $('#is_fullsearch').is(':checked');
                    rows.each(function () {
                        $(this).css('opacity', '0.5');
                    });
                    var value = input.val();

                    if (isFull) {
                        table.search(value, true).draw();
                    } else {
                        table.search(value, false).draw();
                    }
                })
        );

    $('.dataTables_filter').append(
        $('<input>').attr('type', 'checkbox').attr('id', 'is_fullsearch')
    );

    $('.dataTables_filter').append(
        $('<span>').html('Fulltext')
    );

    // Apply the search
    /*table.columns().every(function () {

     var that = this;

     $('input', this.footer()).on('keyup change', function () {
     if (this.value) {
     that.search(this.value).draw();
     }
     });
     });*/

});