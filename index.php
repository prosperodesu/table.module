<?php

use src\router\Router;
use src\database\Connection;

// autoload function
function autoload()
{
    require_once("src/config/Config.php");
    require_once("src/router/BaseRouter.php");
    require_once("src/router/Router.php");
    require_once("src/database/Connection.php");
    require_once("src/models/BaseModel.php");
    require_once("src/models/Table.php");
    require_once("src/cache/Cache.php");
    require_once("src/models/Tasks.php");
    require_once("src/fparse/ParseCsv.php");
    require_once("src/fparse/ParseExcel.php");
    require_once('lib/resolver/Options.php');
    require_once('lib/resolver/OptionsResolver.php');
    require_once('lib/resolver/Exception/ExceptionInterface.php');
    require_once('lib/resolver/Exception/InvalidArgumentException.php');
    require_once('lib/resolver/Exception/InvalidOptionsException.php');
    require_once('lib/resolver/Exception/MissingOptionsException.php');
    require_once('lib/resolver/Exception/NoSuchOptionException.php');
    require_once('lib/resolver/Exception/OptionDefinitionException.php');
    require_once('lib/resolver/Exception/UndefinedOptionsException.php');
    require_once('lib/resolver/Exception/AccessException.php');
    require_once('lib/excel/PHPExcel/IOFactory.php');
    require_once('lib/spreadsheet/Xlsx/AbstractXMLResource.php');
    require_once('lib/spreadsheet/Xlsx/AbstractXMLDictionnary.php');
    require_once('lib/spreadsheet/SpreadsheetParser.php');
    require_once('lib/spreadsheet/SpreadsheetLoaderInterface.php');
    require_once('lib/spreadsheet/SpreadsheetLoader.php');
    require_once('lib/spreadsheet/SpreadsheetInterface.php');
    require_once('lib/spreadsheet/Xlsx/XlsxParser.php');
    require_once('lib/spreadsheet/Xlsx/SpreadsheetLoader.php');
    require_once('lib/spreadsheet/Xlsx/ArchiveLoader.php');
    require_once('lib/spreadsheet/Xlsx/RelationshipsLoader.php');
    require_once('lib/spreadsheet/Xlsx/SharedStringsLoader.php');
    require_once('lib/spreadsheet/Xlsx/StylesLoader.php');
    require_once('lib/spreadsheet/Xlsx/Styles.php');
    require_once('lib/spreadsheet/Xlsx/WorksheetListReader.php');
    require_once('lib/spreadsheet/Xlsx/ValueTransformerFactory.php');
    require_once('lib/spreadsheet/Xlsx/ValueTransformer.php');
    require_once('lib/spreadsheet/Xlsx/DateTransformer.php');
    require_once('lib/spreadsheet/Xlsx/RowIterator.php');
    require_once('lib/spreadsheet/Xlsx/RowIteratorFactory.php');
    require_once('lib/spreadsheet/Xlsx/RowBuilder.php');
    require_once('lib/spreadsheet/Xlsx/RowBuilderFactory.php');
    require_once('lib/spreadsheet/Xlsx/ColumnIndexTransformer.php');
    require_once('lib/spreadsheet/Csv/CsvParser.php');
    require_once('lib/spreadsheet/Csv/RowIterator.php');
    require_once('lib/spreadsheet/Csv/RowIteratorFactory.php');
    require_once('lib/spreadsheet/Csv/Spreadsheet.php');
    require_once('lib/spreadsheet/Csv/SpreadsheetLoader.php');
    require_once('lib/spreadsheet/Xlsx/Archive.php');
    require_once('lib/spreadsheet/Xlsx/ArchiveLoader.php');
    require_once('lib/spreadsheet/Xlsx/Spreadsheet.php');
    require_once('lib/spreadsheet/Xlsx/Relationships.php');
    require_once('lib/spreadsheet/Xlsx/SharedStrings.php');
    require_once('src/fparse/PyHandler.php');
}

autoload();
// test db connection
$db = new Connection();
$router = new Router();
$router->path(parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH));