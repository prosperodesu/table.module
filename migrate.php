<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use src\database\Connection;
use src\database\Migration;

// autoload function
function autoload()
{
    require_once("src/database/Connection.php");
    require_once("src/database/Migration.php");
    require_once("src/config/Config.php");
    require_once("src/router/BaseRouter.php");
    require_once("src/router/Router.php");
}

autoload();

$migration = new Migration();
$migration->migrate();